/*
 * Copyright (C) 2017 Sijan Maharjan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cj.xml;

import cj.xml.platform.XMLDocument;
import cj.xml.platform.XMLQueries;
import org.jdom2.Comment;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * this class helps to create xml file in efficient and effective way
 * @since 1.0
 */
public class XML extends XMLDocument implements XMLQueries {

    private File file;
    private Element rootElement;

    /**
     * initializes with xml file
     * @param file xml file
     */
    public XML(File file) {
        super(file);
        try {
            this.file = file;
            Document document = new SAXBuilder().build(file);
            rootElement = document.getRootElement();
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * initializes with xml doctype and file
     * @param file xml file
     * @param docType doctype
     */
    public XML(File file, Doctype docType){
        this(file);
        super.setDocType(docType);
    }

    /**
     * initializes with file and comment
     * @param file xml file
     * @param comment comment
     */
    public XML(File file, Comment... comment){
        this(file);
        Arrays.stream(comment).forEach(c->{
            super.addContent(c);
        });
    }

    /**
     * initializes with file and comment
     * @param file xml file
     * @param comment comment
     */
    public XML(File file, String comment){
        this(file); int st, en=0;
        while(comment.contains("\n")){
            st = en;
            en = comment.indexOf("\n");
            String p1 = comment.substring(st, en-1);
            super.addContent(new Comment(p1));
            comment = p1+"  "+comment.substring(en+2);
        }
        if(comment != null || comment.equals("")){
            super.addContent(new Comment(comment.trim()));
        }
    }

    /**
     * initializes with xml doctype, comment and file
     * @param file xml file
     * @param docType docType
     * @param comment comment(s)
     */
    public XML(File file, Doctype docType, Comment... comment){
        this(file, comment);
        super.setDocType(docType);
    }

    /**
     * initializes with xml doctype, comment and file
     * @param file xml file
     * @param docType docType
     * @param comment comment
     */
    public XML(File file, Doctype docType, String comment){
        this(file, comment);
        super.setDocType(docType);
    }

    /**
     * invoke to write xml to file
     * @param tag root tag
     */
    public void write(Tag tag){
        try {
            super.setRootElement(tag);
            super.pretifyXML();
            super.commitChanges();
        } catch (IOException e) {
            throw new RuntimeException(e.fillInStackTrace());
        }
    }

    @Override
    public File getFile() {
        return file;
    }

    @Override
    public Element getRoot() {
        return rootElement;
    }

}
