/*
 * Copyright (C) 2017 Sijan Maharjan
 *
 * rootElement program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rootElement program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rootElement program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cj.xml.platform;

import cj.xml.Tag;

import java.util.List;

/**
 * provide list of tags to be added to a tag
 * @see cj.xml.Tag#addAll(Tags)
 */
@FunctionalInterface
public interface Tags {
    /**
     * provide list of tags to be added
     * @return
     */
    List<Tag> getAll();
}
