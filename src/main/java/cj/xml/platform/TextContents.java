/*
 * Copyright (C) 2017 Sijan Maharjan
 *
 * rootElement program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rootElement program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rootElement program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cj.xml.platform;

/**
 * provide text contents for multiple tags
 */
public interface TextContents {
    /**
     * provide text content for tag specified by name and id
     * @param name
     * @param id
     * @return
     */
    String forTag(String name, int id);
}
