/*
 * Copyright (C) 2017 Sijan Maharjan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cj.xml.platform;

import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Sijan Maharjan on 11/7/2017.
 * @since 1.0
 */
public class XMLDocument extends Document {
    private File file;
    private boolean pretify = false;

    /**
     * initialize with file
     * @param file xml file
     */
    public XMLDocument(File file){
        super();
        this.file = file;
    }

    /**
     * creates well structured xml
     */
    public void pretifyXML(){
        pretify = true;
    }

    /**
     * invoke to commit changes to xml
     * @throws IOException exception
     */
    public void commitChanges() throws IOException {
        XMLOutputter xmlOutput = new XMLOutputter();
        if(pretify) {
            xmlOutput.setFormat(Format.getPrettyFormat());
        }
        xmlOutput.output(this, new FileOutputStream(file));
    }
}
