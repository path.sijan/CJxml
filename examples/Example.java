import java.io.File;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

public class Example {

    public static void main(String[] args) {

        //specify xml file
        File xmlFile = new File("hibernate.cfg.xml");

        //create hibernate configuration docType
        Doctype doctype = new Doctype( "hibernate-configuration",
                "-//Hibernate/Hibernate Configuration DTD 3.0//EN",
                "http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd");

        //specify hibernate properties to be added
        Map<String, String> properties = new HashMap<String, String>(){{
            put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
            put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
            put("hibernate.connection.url", "jdbc:mysql://localhost:3306/test");
            put("hibernate.connection.username", "root");
            put("hibernate.connection.password", "update");
            put("hibernate.hbm2ddl.auto", "update");
            put("show_sql", "true");
        }};

        //extract keys as property attributes
        String[] attributes = properties.keySet().toArray(new String[properties.size()]);

        //Start writing xml:
        //create specified xml file (if not exists, else override), including docType and opening comment
        XML xml = new XML(xmlFile, doctype, "auto-generated hibernate configuration file. DON'T EDIT");
        xml.write(                                                                                          //write into xml file :
                Tag.create("hibernate-configuration").add(                                                  //<hibernate-configuration>
                        Tag.create("session-factory").addMultipleTextTags(7, "property",                    //      <session-factory> <property ..> ..
                                (n,i)->Arrays.asList(XMLAttribute.create("name", attributes[i])),
                                (n,i)->properties.get(attributes[i]),
                                null
                        ))
        );
    }
}